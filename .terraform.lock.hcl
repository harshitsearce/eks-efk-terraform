# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.29.0"
  constraints = "~> 3.0"
  hashes = [
    "h1:6UWAfUYbEJo+3R6OS1Ty27GOUwx3oz3xjtuDl7UGB0M=",
    "zh:0e3ec82025efcfed94180858240a1be147bc3eabb24a755f8c58970173b71e54",
    "zh:22897ef5b00317ffa2495a5582c567a4ceca09e9071e0888b18c8364bab6d31b",
    "zh:2e98fc511787045e5ef6f0e76d92ea8de27cd168b20902f9e57d75c96dcb80d8",
    "zh:4e86f7f25c27c139ae17e3ad2a82a154b115d83f16bf8d8fee2aba9f00c80437",
    "zh:71ba0b2b10a5e83b276ebca1d8559354c12656310bfd2554591ac6f0f5541bd0",
    "zh:771989dadb5921bf4586c749a537116eaafdd854e542c5890c9dac55d7b2f8ac",
    "zh:7aa3095c12174b6f8f525ba6007312df6b95de4b4137d25414144e7731ac202c",
    "zh:a1c6f9a6f1abee0cc9c4a3a912c0e6571e7cc439701f03172de44b4187e66769",
    "zh:bd50937e68e9434fc482817e9acfe486b95c69494194627884091a0581a0dffd",
    "zh:e035fd1df86f709374a8547ac141edd1bd899cc7d979b7b28da3ad62fa6ff47b",
  ]
}

provider "registry.terraform.io/mumoshu/eksctl" {
  version     = "0.15.1"
  constraints = ">= 0.3.14"
  hashes = [
    "h1:NHLnm0wVh+XUqiiN+yCSk+4wxWljRRg48s3vWm2WlNE=",
    "zh:029713ac1833b047a5d130a0600dbb34218048e4b8d39616156447c6311d44f7",
    "zh:15ada674c85623e5200a88838377e968e5200d924b06d807d46a9a859ec21aaa",
    "zh:592c753d9b08d863ccee3d05ed3098387fac285d232367a1283dbcdf1445d8ac",
    "zh:62c00a5a66b99650da00904fbecd5d668991c35d7a77d5e45ea3c7a7e4776fcc",
    "zh:8054267ddf51958376c7653ea5abe67c9e65004eb669f477a91da730d6adac52",
    "zh:89ea466a0b642e38daa725552b06622007a57f6646fd7a2bd9b0a87ee51e8604",
    "zh:996d0d12a395556881d15adbaf5e768e447694d507d6b2671da3fe36d0eb56c5",
    "zh:b388d9af55fba88df7609ba4e38d4f5790263182734362f4f5603109a97f6130",
    "zh:d1c6c0489b0baa59365215ee229537f4361837743495992b40311b6c5b9e158d",
    "zh:dcdbbac9b8a4e72c5a58a45d31ab9d1105de31256dd290a40db2541ac84fada5",
  ]
}
