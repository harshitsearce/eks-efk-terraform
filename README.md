
## Prerequisites

We need to install the following modules before starting this demo.

1. awscli: https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html.
2. kubectl: https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html.
3. eksctl: https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html.
4. terraform: https://learn.hashicorp.com/tutorials/terraform/install-cli.
5. Now we need to run the **aws configure** command.
6. The aws access key and secret key will be provided.
7. Set the region to you liking make sure any two people don't have the same region to avoid and confusion.

---

## Creating Resources

Next, We will start creating resources.

1. Clone this repository.
2. Go into the directory and run **terrform init**.
3. Now we will need to make a few changes in each template.
4. We need to make sure that we add and IAM user otherthan us to EKS cluster.
5. We will change the name of some IAM roles (as these are global and unique).
6. Next we are now good to run **terraform plan** [Optionally] we can also play around with variables.tf and change the values to our liking.
7. Once ready we can run the **teraform apply** command. This will go ahead and create all the resources that we need for now.

---