terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
    eksctl = {
      source = "mumoshu/eksctl"
      version = ">= 0.3.14"
    }
  }
}
provider "aws" {
  region = "ap-south-1"
}
