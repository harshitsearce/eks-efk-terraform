data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

resource "aws_security_group" "es" {
  name        = "${var.vpc-name}-elasticsearch-${var.domain}"
  description = "Managed by Terraform"
  vpc_id      = aws_vpc.endowus-vpc.id

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = [
      aws_vpc.endowus-vpc.cidr_block,
    ]
  }
}


resource "aws_elasticsearch_domain" "es" {
  domain_name           = var.domain
  elasticsearch_version = "7.9"

  cluster_config {
    instance_type = "t3.small.elasticsearch"
    instance_count = 2
    zone_awareness_enabled = true
    zone_awareness_config {
        availability_zone_count = 2
    }
  }
  ebs_options {
    ebs_enabled = true
    volume_type = "gp2"
    volume_size = 10
  }
  encrypt_at_rest {
    enabled = true
  }
  node_to_node_encryption {
    enabled = var.advancedsecurityoptions
  }
  domain_endpoint_options {
    enforce_https = var.advancedsecurityoptions
    tls_security_policy = "Policy-Min-TLS-1-0-2019-07"
  }

  vpc_options {
    subnet_ids = [
      aws_subnet.public-subnet1.id,
      aws_subnet.public-subnet2.id,
    ]

    security_group_ids = [aws_security_group.es.id]
  }
  advanced_security_options {
    enabled = var.advancedsecurityoptions
    internal_user_database_enabled = var.advancedsecurityoptions
    master_user_options {
     master_user_name = var.es_masteruser
     master_user_password = var.es_masterpassword
    }
  }

  advanced_options = {
    "rest.action.multi.allow_explicit_index" = "true"
  }

  access_policies = <<CONFIG
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": {
              "AWS": "*"
             },
            "Effect": "Allow",
            "Resource": "arn:aws:es:${var.region}:989618076647:domain/${var.domain}/*"
        }
    ]
}
CONFIG

  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  tags = {
    Domain = var.domain
  }


}