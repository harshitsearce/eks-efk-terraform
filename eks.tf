locals {
  iams = [
    {
      iamarn   = "arn:aws:iam::989618076647:role/Admin-EKS-access"
      username = "Admin-EKS-access"
      groups = [
        "system:masters"
      ]
    },
    {
      iamarn   = "arn:aws:iam::989618076647:user/harshit.sinha@searce.com"
      username = "user-admin"
      groups = [
        "system:masters"
      ]
    },
  ]
}

resource "eksctl_cluster" "endowus_staging_eks" {
  depends_on = [ aws_subnet.public-subnet1 ]
  eksctl_bin = "eksctl.exe"
  name = var.eks_cluster_name
  region = var.region
  spec = <<-EOS
  vpc:
    cidr: ${var.vpc-cidr}       # {optional, must match CIDR used by the given VPC}
    subnets:
      public:
        data.aws_availability_zones.available.names[0]:
          id: ${aws_subnet.public-subnet1.id}
          cidr: ${aws_subnet.public-subnet1.cidr_block} # {optional, must match CIDR used by the given subnet}
        data.aws_availability_zones.available.names[1]:
          id: ${aws_subnet.public-subnet2.id}
          cidr: ${aws_subnet.public-subnet2.cidr_block} # {optional, must match CIDR used by the given subnet}
  iam:
    withOIDC: true
    serviceAccounts: []
  EOS
  dynamic "iam_identity_mapping" {
    for_each = local.iams
    content {
      iamarn   = iam_identity_mapping.value["iamarn"]
      username = iam_identity_mapping.value["username"]
      groups   = iam_identity_mapping.value["groups"]
    }
  }
}

