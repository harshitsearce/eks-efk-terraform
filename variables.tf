variable "vpc-cidr" {
    default = "40.0.0.0/16"
  
}
variable "region" {
  default = "ap-south-1"
}

variable "vpc-name" {
  default = "demo"
}

variable "eks_cluster_name" {
  default = "test"
}

variable "igw_name" {
  default = "demoigw"
}

variable "nat_name" {
  default = "demo_nat"
}

variable "subnet1-name" {
  default = "pu1"
}

variable "subnet2-name" {
  default = "pu2"
}

variable "subnet1-cidr" {
  default = "40.0.0.0/24"
}

variable "subnet2-cidr" {
  default = "40.0.1.0/24"
}

variable "domain" {
  default = "endowus-logging"
}

variable "advancedsecurityoptions" {
  default = "true"
}