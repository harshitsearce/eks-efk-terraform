
#building VPC
resource "aws_vpc" "endowus-vpc" {
  cidr_block = var.vpc-cidr
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = var.vpc-name
    "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
  }
}

# az's for subnets
data "aws_availability_zones" "available" {
  state = "available"
}

#subnets
resource "aws_subnet" "public-subnet1" {
  vpc_id     = aws_vpc.endowus-vpc.id
  cidr_block = var.subnet1-cidr
  availability_zone = data.aws_availability_zones.available.names[0]
  map_public_ip_on_launch = true
 
  tags = {
    Tier = "public"
    Name = var.subnet1-name
    "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
    "kubernetes.io/role/elb" = 1
  }
}
resource "aws_subnet" "public-subnet2" {
  vpc_id     = aws_vpc.endowus-vpc.id
  cidr_block = var.subnet2-cidr
  availability_zone = data.aws_availability_zones.available.names[1]
  map_public_ip_on_launch = true
  tags = {
    Tier = "public"
    Name = var.subnet2-name
    "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
    "kubernetes.io/role/elb" = 1
  }
}
#internetgateway
resource "aws_internet_gateway" "endowus-internetgateway" {
  vpc_id = aws_vpc.endowus-vpc.id
 
  tags = {
    Name = var.igw_name
  }
}
#route_tables
resource "aws_route_table" "public-routetable" {
  vpc_id = aws_vpc.endowus-vpc.id
  tags = {
    Name = "public-routetable"
  }
}
resource "aws_route" "igw_route" {
  route_table_id              = aws_route_table.public-routetable.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.endowus-internetgateway.id
}
resource "aws_route_table_association" "public_subnet1_association" {
  subnet_id      = aws_subnet.public-subnet1.id
  route_table_id = aws_route_table.public-routetable.id
}
resource "aws_route_table_association" "public_subnet2_association" {
  subnet_id      = aws_subnet.public-subnet2.id
  route_table_id = aws_route_table.public-routetable.id
}
